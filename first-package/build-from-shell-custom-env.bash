#!/usr/bin/env bash
nix-shell shell.nix --command 'mkdir build && cd build && cmake .. && make'
