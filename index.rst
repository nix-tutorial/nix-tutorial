Nix tutorial
============

.. warning::

   **This tutorial is not a general purpose Nix tutorial!**

   The goal of this tutorial is to show how useful Nix can be to improve the reproducibility of scientific experiments.
   The audience we had in mind when writing this tutorial is computer scientists that want to control the software environments involved in their experiments.

   Readers interested in general-purpose Nix learning resources can refer to the `nix.dev`_ or NixOS_'s websites, which now contain *getting started* resources, various guides, manuals and detailed information on how Nix works internally.

.. note::

   Any question, remark, or improvement suggestion?
   Feel free to discuss with us on the `nixtm mattermost`_ or open an issue/mr in the `nix tutorial gitlab group`_.

This tutorial presents how Nix_ can help in the making of repeatable experiments.
We propose here to use Nix to define software pieces and to control the environments into which the experiment is run.
Even if this is possible, we chose not to use Nix to store the experiment input and output data in this tutorial.

Please note that Nix is not magic, it cannot do miracles with non-deterministic software pieces and has its limitations.
Here is what Nix can provide to do experiments.

- Fully defined user-space software environments.
  It means you can trivially reenter into the environment from which you run your experiment three years ago: No package version clash nor missing dependency.
- Shareable user-space software environments.
  Define a pure environment on your laptop, then use it smoothly on `Grid'5000`_ clusters or continuous test infrastructures.
- Nix can be used in conjunction with other tools.

  - Full kernel control cannot be achieved with Nix alone (NixOS_ is meant for this), but combining Kameleon_ and Nix makes it possible.
  - Defining a Docker_ image that uses Nix packages is straightforward.

.. toctree::
   :maxdepth: 2

   Installation <installation.rst>
   Getting Started <getting-started.rst>
   Hacking Your First Package <first-package.rst>
   Packaging Your First Experiment <first-experiment.rst>
   Experiment Packaging: DRY <experiment-packaging.rst>
   Working with Notebooks <notebook.rst>
   Deploying and Binary Cache <deploying.rst>
   Convenient Management of Inputs <flakes.rst>
   Resources to Go Further <going-further.rst>

.. todolist::

.. _Nix: https://nixos.org/nix/
.. _NixOS: https://nixos.org/nixos/
.. _nix.dev: https://nix.dev/
.. _Grid'5000: https://www.grid5000.fr/w/Grid5000:Home
.. _Kameleon: http://kameleon.imag.fr/
.. _Docker: https://en.wikipedia.org/wiki/Docker_(software)

.. _nixtm mattermost: https://framateam.org/signup_user_complete/?id=h81asczhufgmfxgb8afxntq7te
.. _nix tutorial gitlab group: https://gitlab.inria.fr/nix-tutorial
