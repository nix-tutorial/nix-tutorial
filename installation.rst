Installation
============

Installing Nix
--------------

On Your Laptop
''''''''''''''

The installation procedure can be found on `Nix's front page <https://nixos.org/nix>`_.
In most scenarios, running this single command **as normal user** should install Nix.

.. code-block:: bash

  sh <(curl -L https://nixos.org/nix/install) --daemon

.. warning::
  If the installation fails with a ``error: cloning builder process: Operation not permitted`` message, your kernel probably prevents the Nix installer to use Nix's sandboxing facilities.
  In short, by default, Nix isolate builds in `chroot`-like environments to make sure they cannot access undeclared (out-of-Nix) dependencies.
  For example, a build script that links against ``/usr/lib/libevent.so`` would fail.

  A first workaround is to disable Nix's sandboxing mechanism.
  This is done by configuring ``/etc/nix/nix.conf`` as follows (should be run **as root**).

  .. code-block:: bash

    mkdir -p /nix /etc/nix
    chmod a+rwx /nix
    echo 'sandbox = false' > /etc/nix/nix.conf

  An alternative and better solution is to configure your kernel so that sandboxing can work.
  This is usually not required as Nix's sandboxing should work with default kernel options, but some distributions disable certain features — e.g., Debian disables user namespaces.
  Here is a Debian-specific workaround to enable user namespaces (should be run **as root**).

  .. code-block:: bash

    echo 1 > /proc/sys/kernel/unprivileged_userns_clone


On Grid'5000
''''''''''''

First, connect to a Grid'5000 frontend (refer to `Grid'5000 Getting Started <https://www.grid5000.fr/w/Getting_Started>`_ if needed).

Then, create a job thanks to ``oarsub`` — here, a 3-hour length job to match the tutorial duration.

.. code-block:: sh

  oarsub -I -l "walltime=3:00:00"

You should now be inside your job and able to install Nix as explained in `On Your Laptop`_.
The following script installs Nix.

.. code-block:: bash

  sudo-g5k
  sh <(curl -L https://nixos.org/nix/install) --daemon

Final Steps
'''''''''''

Nix has been installed but it is not accessible on your environment yet, which means you cannot access Nix binaries such as ``nix-shell`` and ``nix-env``.
The simplest way to achieve this is to start a new shell session, for example by calling ``bash`` if you use Bash_ or ``zsh`` if you use Zsh_.

You can check your installation by calling one of the nix commands.
Running ``nix-build --version`` should for instance output ``nix-build (Nix) 2.9.2``.

Some of the commands used in this tutorial use the ``nix`` binary, which is a quite recent program not enabled by default.
You can enable the ``nix`` command (and the flakes_ features that goes with it) by enabling experimental features in your Nix configuration file.

.. code-block:: bash

   echo 'experimental-features = nix-command flakes' >> /etc/nix/nix.conf

.. note::
  If you want this configuration to persist over different Grid'5000 jobs, you can set this configuration option in the ``${HOME}/.config/nix/nix.conf`` file instead.

Uninstalling Nix
----------------

This is straightforward, as all Nix packages are in the Nix store.

#. Delete the Nix store (and misc. Nix-related files) **as root**: ``rm -rf /nix/``
#. (Delete the Nix configuration file **as root**: ``rm -rf /etc/nix``)
#. Delete your user links **as normal user**: ``rm -rf ~/.nix-channels ~/.nix-defexpr ~/.nix-profile``

.. _Bash: https://en.wikipedia.org/wiki/Bash_(Unix_shell)
.. _Zsh: https://en.wikipedia.org/wiki/Z_shell
.. _flakes: https://www.tweag.io/blog/2020-05-25-flakes/
