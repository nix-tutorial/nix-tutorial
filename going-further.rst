Resources to Go Further
=======================

#. `NixOS's website`_ contains the official and up-to-date Nix documentation. It includes various manuals and guides: Entry-level tutorials, and tutorials that go deeper about how Nix works such as `Nix pills`_.
#. learnNixInYMinutes_ gives a dense overview of the Nix Expression Language.
#. Exploring `Nixpkgs`_ functions and packages can be a great source of inspiration.
#. Exploring simpler package repositories such as kapack_ can also be a good source of inspiration.

Otherwise, here is random list of Nix-related resources.

- `nix-user-chroot`_ enables to use Nix without root permissions (*e.g.,* on a Grid'5000 frontend).
- Lorri_ is a nix-shell replacement that tries to maximize comfort while developing projects.
- `NixOS wiki on pinning Nixpkgs`_
- `NixOS wiki on customizing packages`_
- `Nix file naming conventions`_
- `Information about channels`_
- `Channels and reproducibility`_

.. _NixOS's website: https://nixos.org
.. _Nix pills: https://nixos.org/nixos/nix-pills/
.. _learnNixInYMinutes: https://learnxinyminutes.com/docs/nix/
.. _Nixpkgs: https://github.com/NixOS/nixpkgs
.. _kapack: https://github.com/oar-team/nur-kapack/
.. _Lorri: https://github.com/nix-community/lorri
.. _nix-user-chroot: https://github.com/nix-community/nix-user-chroot
.. _NixOS wiki on pinning Nixpkgs: https://nixos.wiki/wiki/FAQ/Pinning_Nixpkgs
.. _Nix file naming conventions: https://stackoverflow.com/questions/44088192/when-and-how-should-default-nix-shell-nix-and-release-nix-be-used
.. _Channels and reproducibility: https://matrix.ai/blog/intro-to-nix-channels-and-reproducible-nixos-environment/
.. _Information about channels: https://matthewbauer.us/blog/channel-changing.html
.. _NixOS wiki on customizing packages: https://nixos.org/nixos/manual/index.html#sec-customising-packages
