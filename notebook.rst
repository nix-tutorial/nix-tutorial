Working with notebooks
======================

Notebooks can be valuable assets for experimenters, notably since they can bind results and their analysis together.
This section shows how some notebook technologies can be used in conjunction with Nix, thus improving the notebook reproducibility.

Jupyter notebook
----------------

Jupyter_ is a popular web-based Python_ notebook, formerly called *IPython Notebook*.
The example is stored in the ``notebook-jupyter`` directory of the `Chord experiments git repository`_.
The directory contains a Jupyter file and the following Nix shell.

.. literalinclude:: ./examples/chord-experiments/notebook-jupyter/shell.nix
  :caption: :download:`notebook-jupyter/shell.nix <./examples/chord-experiments/notebook-jupyter/shell.nix>`
  :language: nix

This shell points to the package repository we have done in section :ref:`section_expe_packaging_do_not_repeat_yourself` and defines a single shell from which all desired packages are set.
This example introduces a Python_ environment in Nix, which is here done thanks to ``python?.withPackages``, which in short defines a python interpreter with a set of available packages.
Please refer to `NixOS's wiki on Python`_ and `Nix's official Python documentation`_ for more information about using Python and Nix.

Running jupyter simply consists in running ``nix-shell`` in the ``notebook-jupyter`` directory.
This should run a Jupyter server, which will also (in a non-pure shell) run your brower so you can interact with Jupyter.

R markdown
----------

`R markdown`_ is a popular R_ notebook that can be used with or without RStudio_.
The example is stored in the ``notebook-rmarkdown`` directory of the `Chord experiments git repository`_.
This directory contains a R markdown file, a nix-shell and a runner script.
More information on using R with Nix can be found on `NixOS wiki's page on R`_.

.. literalinclude:: ./examples/chord-experiments/notebook-rmarkdown/shell.nix
  :caption: :download:`notebook-rmarkdown/shell.nix <./examples/chord-experiments/notebook-rmarkdown/shell.nix>`
  :language: nix

.. literalinclude:: ./examples/chord-experiments/notebook-rmarkdown/runner.R
  :caption: :download:`notebook-rmarkdown/runner.R <./examples/chord-experiments/notebook-rmarkdown/runner.R>`
  :language: r

As the runner uses a nix-shell shebang, generating the notebook output is done like this.

.. code-block:: bash

   ./runner.R

Emacs's org-mode
----------------
`org-mode`_ is a popular `GNU Emacs`_ package that can be used as a notebook technology.
The example is stored in the ``notebook-org-mode`` directory of the `Chord experiments git repository`_.
The directory contains an org-mode file, and a nix-shell.
More information on using Emacs with Nix can be found on `NixOS manual on Emacs`_.

.. literalinclude:: ./examples/chord-experiments/notebook-org-mode/shell.nix
  :caption: :download:`notebook-org-mode/shell.nix <./examples/chord-experiments/notebook-org-mode/shell.nix>`
  :language: nix

Emacs can be executed on the org-mode file with the following command.

.. code-block:: bash

    nix-shell --pure shell.nix --command 'emacs -q ./experiment.org'

Once in Emacs, exporting the notebook can be done with the convenient ``C-c C-e h h`` Emacs shortcut.

.. _Chord experiments git repository: https://gitlab.inria.fr/nix-tutorial/chord-experiments
.. _Jupyter: https://jupyter.org/
.. _Python: https://www.python.org/
.. _NixOS's wiki on Python: https://nixos.wiki/wiki/Python
.. _Nix's official Python documentation: https://nixos.org/nixpkgs/manual/#python
.. _R: https://www.r-project.org/
.. _R markdown: https://rmarkdown.rstudio.com/
.. _RStudio: https://www.rstudio.com/
.. _NixOS wiki's page on R: https://nixos.wiki/wiki/R
.. _org-mode: https://www.orgmode.org/
.. _GNU Emacs: https://www.gnu.org/software/emacs/
.. _NixOS manual on Emacs: https://nixos.org/nixos/manual/index.html#module-services-emacs
