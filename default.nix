{ pkgs ? import
  ( fetchTarball "https://github.com/NixOS/nixpkgs/archive/19.03.tar.gz") {}
}:

let
  pythonPackages = pkgs.python37Packages;
  buildPythonPackage = pythonPackages.buildPythonPackage;

  packages = rec {
    sphinx_doc = pkgs.stdenv.mkDerivation rec {
      pname = "tuto-nix-doc";
      version = "0.1.0";

      src = ./.;
      buildInputs = with pythonPackages; [ sphinx sphinx_rtd_theme ];

      buildPhase = "make html";
      installPhase = ''
        mkdir -p $out
        cp -r _build/html $out/
      '';
    };
  };
in
  packages.sphinx_doc
